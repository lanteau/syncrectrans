CXX = g++
INCLUDE = -I./include
CXXFLAGS = $(INCLUDE) -O2 -pipe -std=c++11
VPATH = ./src
OBJDIR = ./src
TRANS_OBJS = SyncTransmitter.o TCPSocket.o TCPServer.o Message.o \
				FileReader.o ErrorModule.o CRC.o FECModule.o cmdline.o \
				config_file.o convert.o options_description.o parsers.o \
				positional_options.o split.o utf8_codecvt_facet.o value_semantic.o \
				variables_map.o winmain.o
RECV_OBJS = SyncReceiver.o TCPSocket.o TCPClient.o Message.o \
				CRC.o FECModule.o cmdline.o config_file.o convert.o \
				options_description.o parsers.o positional_options.o split.o \
				utf8_codecvt_facet.o value_semantic.o variables_map.o winmain.o

OUT_TRANS_OBJS = $(addprefix $(OBJDIR)/,$(TRANS_OBJS))
OUT_RECV_OBJS = $(addprefix $(OBJDIR)/,$(RECV_OBJS))

EXEC1 = SyncTransmitter
EXEC2 = SyncReceiver

all: $(EXEC1) $(EXEC2)

debug: CXXFLAGS += -ggdb
debug: $(EXEC1) $(EXEC2)

$(EXEC1): $(OUT_TRANS_OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXEC1) $(OUT_TRANS_OBJS)

$(EXEC2): $(OUT_RECV_OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXEC2) $(OUT_RECV_OBJS)
$(OBJDIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f $(OBJDIR)/*.o $(EXEC1) $(EXEC2)