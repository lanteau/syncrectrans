#include "TCPSocket.h"
#include "TCPClient.h"
#include "Message.h"
#include <string>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iostream>
#include <cstring>
#include "ErrorModule.h"
#include <memory>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char** argv)
{
	bool enableCRC = false;
	bool enableHamming = false;
	bool showErrors = false;
	std::string hostname;
	int port;

	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "print this help message")
		("host,h", po::value<std::string>(&hostname)->required(), "set ip address of host server to connect to")
		("port,p", po::value<int>(&port)->required(), "set port number of server to connect to")
		("crc", "enable CRC error checking")
		("hamming", "enable hamming for single bit error correction")
		("show-errors", "display positions of corrected errors");

	po::variables_map vm;
	try
	{
		po::store(po::parse_command_line(argc, argv, desc), vm);

		// Handle the help option before running notify() so that help
		// runs without required options
		if (vm.count("help"))
		{
			std::cout << desc << "\n";
			return 1;
		}
		po::notify(vm);
	}
	catch(po::error& e)
	{
		std::cerr << e.what() << ", please see --help" << std::endl;
		return 1;
	}

	if(vm.count("crc")) enableCRC = true;
	if(vm.count("hamming")) enableHamming = true;
	if(vm.count("show-errors"))	showErrors = true;

	int receivedLen;
	std::string receivedData;
	char buffer[256];
	memset(&buffer, 0, sizeof(buffer));

	std::unique_ptr<TCP::TCPClient> client(new TCP::TCPClient());
	std::unique_ptr<TCP::TCPSocket> socket(client->connect(hostname.c_str(), port));

	bool received = false;
	char ack = 0x6; //ACK character is ASCII 6
	char nack = 0x15;
	int numPackets = 0;

	std::vector<error_location> allErrors;

	if(socket)
	{	
		while(!received)
		{
			receivedLen = socket->receive(buffer, sizeof(buffer));
			if(receivedLen > 0)
			{
				if(receivedLen > 2) //This is a block
				{
					Message newMessage(buffer, receivedLen, enableCRC, 
						enableHamming);
					errorPairVec errors = newMessage.getErrorsVec();

					for(size_t i = 0; i < errors.size(); i++)
					{
						error_location e;
						e.msgNum = numPackets;
						e.byteNum = errors.at(i).first;
						e.bitNum = errors.at(i).second;
						allErrors.push_back(e);
					}

					if(newMessage.isValid()) 
					{
						socket->send(&ack, sizeof(ack));
						numPackets++;
						receivedData.append(newMessage.getDataString());
					}
					else
					{
						socket->send(&nack, sizeof(nack));
						std::cerr << "CRC check on frame number " << std::dec <<
						 numPackets << " failed! Asking for retransmission...\n"; 
					}
					
				}
				if(buffer[0] == 4) //EOT
				{
					received = true;
					socket->close();
				}
			}
		}

		std::cout << "Received message was:" << std::endl << std::endl 
			<< receivedData << std::endl << std::endl;
		std::cout << "Number of packets received: " << numPackets << std::endl;

		if(showErrors)
			for(size_t i = 0; i < allErrors.size(); i++)
			{
				error_location e = allErrors.at(i);
				std::cerr << "Error " << i << std::endl << "Message: " << e.msgNum 
					<< std::endl << "Byte num: " << e.byteNum << std::endl 
					<< "Bit num: " << e.bitNum << std::endl << std::endl;
			}

		return 0;
	}
	return 1;
}