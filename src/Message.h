/* Message class
*
*	A Message object represents a single "block" of data in our protocol. Each Message
* starts with 2 ASCII SYN characters, then a byte representing length of the data to
* follow (1-64 bytes). Finally, the data itself that is between 1 and 64 bytes in
* length. If hamming is used the data will be expanded to 128 bytes. If CRC is used 2
* additional bytes will be added for CRC-16.
*
* Three constructors, First constructs a message object from a std::string and frames the
* data by adding control characters etc, this is then stored in m_rawMessage. The
* second constructor takes a char buffer which will contain raw data, and removes all
* the framing and stores the data result in m_data. Third is empty constructor.
*
* Public methods:
* getDataLength() returns length of m_data
* getFrameLength() returns length of m_rawMessage
* isValid() returns m_valid, result of running verify() which does CRC verification
* getDataString() returns a copy of m_data
* getRawString() returns a copy of m_rawMessage
* getErrorsVec() returns a vector of error pairs (byte, bit) of errors fixed by
* 	FEC(hamming)
* setRawString() resets m_rawMessage, this is used by the ErrorModule class
*
* Private methods:
* frame() adds all the control bytes, CRC, etc onto the data. Runs encode() for
* 	FEC. Puts end product in m_rawMessage.
* deFrame() does the opposite of frame(), returns actual data into m_data
* verify() runs CRC verification on received message
*
* Author: Logan Anteau
*/
#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include "FECModule.h"

class Message
{
	std::string m_rawMessage; // contains actual message plus extra control characters, etc
	std::string m_data;
	errorPairVec m_errors;

	bool m_valid;
	bool m_crc;
	bool m_hamming;
	
public:
	Message(std::string data, bool crc, bool hamming);
	Message(char* buffer, int len, bool crc, bool hamming);
	Message();

	size_t getDataLength();
	size_t getFrameLength();
	bool isValid(); // Returns true if byte is verified by odd parity check
	std::string getDataString();
	std::string getRawString();
	errorPairVec getErrorsVec();
	void setRawString(std::string newString);


private:
	void frame();
	void deFrame();
	void verify();

	bool calculateOddParity(unsigned char byte);
};
#endif /* MESSAGE_H */