#include "ErrorModule.h"
#include <random>
#include <bitset>

ErrorModule::ErrorModule(std::vector<Message> &msgs) :
	m_messages(msgs)
{
}

std::vector<error_location> ErrorModule::createErrors(int num)
{
	std::vector<error_location> errors;

	std::random_device rd; // hardware random number generator
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> msgDist(0, m_messages.size() - 1);
	std::uniform_int_distribution<> bitDist(0, 7);

	// Return vector with erroneous location if num <= 0
	if(num <= 0)
	{
		error_location e;
		e.msgNum = -1;
		e.byteNum = -1;
		e.bitNum = -1;
		errors.push_back(e);
		return errors;
	}

	int i = 0;

	while(i < num)
	{
		error_location e;
		e.msgNum = msgDist(eng);
		Message &m = m_messages.at(e.msgNum);
		std::string rawData = m.getRawString();

		std::uniform_int_distribution<> byteDist(0, rawData.length() - 1 );
		e.byteNum = byteDist(eng);

		// unique is used to prevent 2 different "error locations" to be on the same message, same byte
		bool unique = true;
		for(size_t i = 0; i < errors.size(); i++)
		{
			if(errors.at(i).msgNum == e.msgNum && errors.at(i).byteNum == e.byteNum)
				unique = false;
		}

		if(unique)
		{
			e.bitNum = bitDist(eng);
			unsigned char &byte = reinterpret_cast<unsigned char&>(rawData.at(e.byteNum));
			std::bitset<8> bits(byte);

			if(bits[e.bitNum])
				bits[e.bitNum] = 0;
			else
				bits[e.bitNum] = 1;

			unsigned long j = bits.to_ulong(); 
			byte = static_cast<unsigned char>(j);

			m.setRawString(rawData);

			errors.push_back(e);
			i++;
		}	
	}

	return errors;
}