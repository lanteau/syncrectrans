/* FECModule class
*
* This class creates an object that is used to implement FEC
* (Forward Error Correction) on the message in the form of hamming. [7,4]Hamming
* is used, so each byte of data is separated into 2 nibbles (4 bits). Each
* nibble then gets 3 parity bits added in positions 1, 2, and 4. The unused 8th
* bit of the new byte is used for an overall odd parity of the preceding 7 bits.
*
* encode() takes a string as input and returns a string twice in length with the
* added parity.
*
* decode() takes a string with the added parity and finds an fixes errors and
* strips away parity data, returning the original data string passed into encode().
*
* getErrors() returns a std::vector of byte,bit pairs of detected (and corrected) 
* errors.
*
* Author: Logan Anteau
*/
#ifndef _FECMODULE_H
#define _FECMODULE_H
#include <cstdint>
#include <vector>
#include <cstdlib>
#include <string>
#include <utility>

typedef std::pair<int, int> errorPair;
typedef std::vector<errorPair> errorPairVec;

class FECModule
{
	static std::vector<uint16_t> m_codeVec;
	errorPairVec m_errors;

	void init();
	int findError(unsigned char byte);
	void fixError(unsigned char *byte, int errPos);

public:
	FECModule();
	std::string encode(std::string &input);
	std::string decode(std::string &input);
	errorPairVec getErrors();
};
#endif /* _FECMODULE_H */