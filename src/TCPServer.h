/* TCPServer class
*
* TCPServer represents a server running on a TCP connection
*  
* constructor takes a port number an optional string for the hostname to listen
* on. If no IP is given, it will listen on INADDR_ANY
*
* start() binds to and starts listening on a socket
* 
* accept() begins accepting connections. This call blocks until a connection is
* made, then returns a pointer to a TCPSocket of connected client
*
* Author: Logan Anteau
*/

#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <string>

namespace TCP
{
	class TCPServer
	{
		int m_listenSD;
		int m_portNum;
		std::string m_address;
		bool m_listening;

	public:
		TCPServer(int port, const char* address="");
		~TCPServer();

		int start();

		TCPSocket* accept(); 
		
	private:
		TCPServer() {}
	};
}
#endif /* TCPSERVER_H */