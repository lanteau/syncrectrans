/* ErrorModule class is used to inject single bit errors into Messages
*
* constructor takes a reference to a vector of Message's as an input.
*
* createErrors() introduces num errors into various messages, bytes, and bits
* in the vector m_messages
*
* Author: Logan Anteau
*/
#ifndef _ERRORMODULE_H
#define _ERRORMODULE_H
#include <vector>
#include "Message.h"

struct error_location
{
	int msgNum;
	int byteNum;
	int bitNum;
};

class ErrorModule
{
	std::vector<Message> &m_messages;

public:
	ErrorModule(std::vector<Message> &msgs);
	std::vector<error_location> createErrors(int num);

};
#endif /* _ERRORMODULE_H */