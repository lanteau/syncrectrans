/* TCPClient class
*
* TCPClient is as the name suggests a class representing a TCPClient. The
* TCPClient only has connect() and resolveHost() methods. The connect() method
* returns a pointer to a TCPSocket that can then be used to transmit data
*
* Author: Logan Anteau
*/
#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "TCPSocket.h"

namespace TCP
{
	class TCPClient
	{

	public:
		TCPSocket* connect(const char* hostname, int port);

	private:
		//Resolve hostname to an ip address
		int resolveHost(const char* hostname, struct in_addr* addr); 

	};
}
#endif /* TCPCLIENT_H */