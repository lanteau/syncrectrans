#include "TCPSocket.h"
#include "TCPServer.h"
#include "Message.h"
#include "FileReader.h"
#include "ErrorModule.h"
#include <string>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cerrno>
#include <unistd.h>
#include <memory>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char** argv)
{
	int portNum = 0;
	int numErrors = 0;
	std::string hostname;
	std::string filename;
	bool enableCRC = false;
	bool enableHamming = false;
	bool showErrors = false;

	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "print this help message")
		("filename,f", po::value<std::string>(&filename)->required(), "filename of file to transmit over network.")
		("host,h", po::value<std::string>(&hostname), "IP address to listen for connections on. INADDR_ANY if no IP given.")
		("port,p", po::value<int>(&portNum)->required(), "port number to listen for connections on")
		("numErrors,n", po::value<int>(&numErrors), "number of single bit errors to introduce into transmission (default is 0)")
		("crc", "enable CRC error checking")
		("hamming", "enable hamming for single bit error correction")
		("show-errors", "display positions of introduced errors");

	po::variables_map vm;
	try
	{
		po::store(po::parse_command_line(argc, argv, desc), vm);

		// Handle the help option before running notify() so that help
		// runs without required options
		if (vm.count("help"))
		{
			std::cout << desc << "\n";
			return 1;
		}
		po::notify(vm);
	}
	catch(po::error& e)
	{
		std::cerr << e.what() << ", please see --help" << std::endl;
		return 1;
	}

	if(vm.count("crc")) enableCRC = true;
	if(vm.count("hamming")) enableHamming = true;
	if(vm.count("show-errors"))	showErrors = true;

	std::unique_ptr<TCP::TCPSocket> socket;
	std::unique_ptr<TCP::TCPServer> server;

	FileReader myFile(filename, enableCRC, enableHamming);
	try
	{
		myFile.parse();
	}
	catch(int e)
	{
		std::cerr << "Could not parse input file: " << strerror(e) << std::endl;
		return 2;
	}

	std::vector<Message> messages = myFile.getMessages();
	std::vector<Message> origMessages(messages);

	ErrorModule em(messages);
	std::vector<error_location> errors = em.createErrors(numErrors);
	
	if(showErrors)	
		for(size_t i = 0; i < errors.size(); i++)
		{
			error_location e = errors.at(i);
			if(e.msgNum > -1)
			{
				std::cerr << "Error number " << i << std::endl;
				std::cerr << "Message num: " << e.msgNum << std::endl;
				std::cerr << "Byte num: " << e.byteNum << std::endl;
				std::cerr << "Bit num: " << e.bitNum + 1 << std::endl << std::endl;
			}
		}
	
	if(portNum < 1025)
	{
		std::cerr << "Error: port number must be above 1024" << std::endl;
		return 3;
	} 

	if(hostname.length())
		server = std::unique_ptr<TCP::TCPServer>(new TCP::TCPServer(portNum, hostname.c_str()));
	else
		server = std::unique_ptr<TCP::TCPServer>(new TCP::TCPServer(portNum));

	if(server->start() == 0)
	{
		std::cout << "Listening connections on port " << portNum << "..." << std::endl;

		while(1)
		{
			socket = std::unique_ptr<TCP::TCPSocket>(server->accept());
			if(socket)
			{
				int pid = fork();
  				if(pid < 0)
  				{
  					std::cerr << "Error on fork: " << strerror(errno) << std::endl;
  					return(1);
  				}
  				if(pid == 0)
  				{
					unsigned int i = 0;
					unsigned int resends = 0;
					bool resend = false;

					Message currMsg;

					while(i < messages.size() && resends < (errors.size() + 100))
					{
						if(!resend)
							currMsg = messages.at(i);
						else
						{
							currMsg = origMessages.at(i);
							resend = false;
						}
						char buffer[256];
						char receiveBuffer[256];
						memset(&buffer, 0, sizeof(buffer)); // Clear buffer

						if(currMsg.getFrameLength() > 256)
							return 1;
						currMsg.getRawString().copy(&buffer[0], currMsg.getFrameLength());

						socket->send(buffer, currMsg.getFrameLength());
						socket->receive(receiveBuffer, sizeof(receiveBuffer));

						if(receiveBuffer[0] == 0x6) //ACK character
						{
							i++;
							receiveBuffer[0] = 0;
						}
						else if(receiveBuffer[0] == 0x15) //NACK character
						{
							resend = true;
							resends++;
						}
					}
			
					char endOfTrans = 0x4; //EOT
					socket->send(&endOfTrans, sizeof(endOfTrans));
					std::cout << "Number of resent blocks: " << resends << std::endl;
					socket->close(); //Close connection to client
					return (0);
				}
				else
				{
					socket->close();
				}
			}
		}
	}
	std::cerr << "Could not start Transmit server: " << strerror(errno) << std::endl;
	return(-1);
}
