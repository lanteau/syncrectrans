#include "FECModule.h"
#include <bitset>

std::vector<uint16_t> FECModule::m_codeVec;

FECModule::FECModule()
{
	if(!(FECModule::m_codeVec.size() > 0))
		init();
}

int FECModule::findError(const unsigned char byte)
{
	if(!byte)
		return 0;

	std::bitset<8> b(byte);

	bool p1 = byte & 0x1;
	bool p2 = byte & 0x2;
	bool p4 = byte & 0x8;
	bool p0 = byte & 0x80;

	bool calcP1 = b[2] ^ b[4] ^ b[6];
	bool calcP2 = b[2] ^ b[5] ^ b[6];
	bool calcP4 = b[4] ^ b[5] ^ b[6];
	bool calcP0 = false;
	for(size_t i = 0; i < 7; i++)
		calcP0 ^= b[i];

	short errPos = 0;

	if(calcP1 ^ p1)
		errPos += 1;
	if(calcP2 ^ p2)
		errPos += 2;
	if(calcP4 ^ p4)
		errPos += 4;

	if(calcP0 ^ p0 && !errPos)
		errPos = 8;

	return errPos;
}

void FECModule::fixError(unsigned char *byte, int errPos)
{
	*byte ^= 1 << (errPos - 1);
	return;
}

errorPairVec FECModule::getErrors()
{
	return m_errors;
}

std::string FECModule::encode(std::string &input)
{
	std::string output;

	for(size_t i = 0; i < input.length(); i++)
	{
		char c = input[i];
		uint16_t twoBytes = m_codeVec.at(reinterpret_cast<unsigned char&>(c));

		char lowByte = twoBytes & 0xFF;
		char highByte = (twoBytes & 0xFF00) >> 8;

		output.push_back(lowByte);
		output.push_back(highByte);
	}
	return output;
}

std::string FECModule::decode(std::string &input)
{
	std::string output;

	for(size_t i = 0; i < input.length(); i = i+2)
	{
		unsigned char lowByte = reinterpret_cast<unsigned char&>(input[i]);
		int errPos = findError(lowByte);

		if(errPos)
		{
			m_errors.push_back(errorPair(i, errPos));
			fixError(&lowByte, errPos);
		}

		unsigned char highByte = reinterpret_cast<unsigned char&>(input[i+1]);
		errPos = findError(highByte);
		if(errPos)
		{
			m_errors.push_back(errorPair(i+1, errPos));
			fixError(&highByte, errPos);
		}

		unsigned char dataByte = 0;
		dataByte |= ((lowByte & 0x4) >> 2); // lowByte & 00000100
		dataByte |= ((lowByte & 0x10) >> 3); // lowByte & 00010000
		dataByte |= ((lowByte & 0x20) >> 3); // lowByte & 00100000
		dataByte |= ((lowByte & 0x40) >> 3); //lowByte & 01000000

		dataByte |= ((highByte & 0x4) << 2);
		dataByte |= ((highByte & 0x10) << 1);
		dataByte |= ((highByte & 0x20) << 1);
		dataByte |= ((highByte & 0x40) << 1);

		output.push_back(dataByte);
	}
	return output;
}

void FECModule::init()
{
	for(size_t i = 0; i < 256; i++)
	{
		char lowNibble = i & 0xF;
		char highNibble = (i & 0xF0) >> 4;
		uint16_t codeWord = 0;

		char currNibble = lowNibble;
		for(size_t j = 0; j < 2; j++)
		{
			// Set data into correct positions
			std::bitset<8> byte;
			byte[2] = currNibble & 0x1;
			byte[4] = currNibble & 0x2;
			byte[5] = currNibble & 0x4;
			byte[6] = currNibble & 0x8;

			bool p1 = false;
			bool p2 = false;
			bool p4 = false;
			bool overall = false;

			//Calculate p1
			p1 ^= byte[2];
			p1 ^= byte[4];
			p1 ^= byte[6];
			byte.set(0, p1);

			//Calculate p2
			p2 ^= byte[2];
			p2 ^= byte[5];
			p2 ^= byte[6];
			byte.set(1, p2);

			//Caclulate p4
			p4 ^= byte[4];
			p4 ^= byte[5];
			p4 ^= byte[6];
			byte.set(3, p4);

			//Calculate overall parity bit
			overall = false;
			for (size_t k = 0; k < 7; k++)
				overall ^= byte[k];
			byte.set(7, overall);

			// Put result in codeWord
			codeWord += (byte.to_ulong() << (j * 8));

			currNibble = highNibble;
		}

		m_codeVec.push_back(codeWord);
	}
	return;
}