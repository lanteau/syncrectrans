#include "FileReader.h"
#include <fstream>
#include <cerrno>

FileReader::FileReader(std::string fileName, bool crc, bool hamming) :
	m_fileName(fileName),
	m_crc(crc),
	m_hamming(hamming)
{
}

void FileReader::changeFile(std::string fileName)
{
	m_fileName = fileName;
	m_fileData.empty();
}

void FileReader::parse()
{
	std::ifstream in(m_fileName.c_str(), std::ios::in | std::ios::binary);

	if(in)
	{
		in.seekg(0, std::ios::end);
		m_fileData.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&m_fileData[0], m_fileData.size());
		in.close();

	}
	else
		throw(errno);
	
	return;
}

std::string FileReader::getFileString()
{
	return m_fileData;
}

FileReader::MessageVec FileReader::getMessages()
{
	MessageVec messages;
	size_t dataPos = 0;

	//Calc number of frames
	size_t numFrames = m_fileData.length() / 64;
	if(m_fileData.length() % 64 > 0)
		numFrames++;

	for(size_t i = 0; i < numFrames; i++)
	{
		//Calculate length of frame
		int frameLen = 64;
		if(dataPos+64 > m_fileData.length())
			frameLen = m_fileData.length() - dataPos;

		std::string newMessageStr = m_fileData.substr(dataPos, frameLen);
		Message msg(newMessageStr, m_crc, m_hamming);
		messages.push_back(msg);

		dataPos += frameLen;
	}

	return messages;
}