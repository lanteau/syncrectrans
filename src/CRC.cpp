#include "CRC.h"

CRC::CRC()
{
	init();
}

void CRC::process_bytes(char *message, int nBytes)
{
	// CRC-ANSI
	// Pattern: X^16 + X^15 + X^2 + 1 (11000000000000101) truncated to 1000000000000101

	if(!m_crcVec.size())
		init();

	m_checksum = 0;
	uint8_t data = 0;

	for (int byte = 0; byte < nBytes; ++byte)
	{
		data = reinterpret_cast<unsigned char&>(message[byte]) ^ (m_checksum >> (WIDTH - 8));
		m_checksum = reinterpret_cast<unsigned char&>(m_crcVec.at(data)) ^ (m_checksum << 8);
	}
	return;
}

uint16_t CRC::checksum()
{
	return m_checksum;
}

void CRC::init()
{
	uint16_t remainder;
	for (int dividend = 0; dividend < 256; ++dividend)
	{
		remainder = dividend << (WIDTH - 8);

		for (uint8_t bit = 8; bit > 0; --bit)
		{	
			if (remainder & TOPBIT)
				remainder = (remainder << 1) ^ POLYNOMIAL;
			else
				remainder = (remainder << 1);
		}

		m_crcVec.push_back(remainder);
	}
	return;
}