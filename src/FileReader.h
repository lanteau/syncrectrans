/* FileReader class
*
*	FileReader object takes a filename in it's constructor. This file is then read
* into an internal string using the parse() method. A string containing the
* the file's data can be obtained with getFileString(). A vector of Messages
* can also be obtained with getMessages().
*
* Author: Logan Anteau
*/

#ifndef FILEREADER_H
#define FILEREADER_H

#include "Message.h"
#include <string>
#include <vector>

class FileReader
{
	std::string m_fileName;
	std::string m_fileData;
	bool m_crc;
	bool m_hamming;

	typedef std::vector<Message> MessageVec;

public:

	FileReader(std::string fileName, bool crc, bool hamming);

	//Changes FileReader to look at a new file
	void changeFile(std::string fileName);

	//Reads the input file into m_fileData
	void parse();

	//Returns string containing contents of file
	std::string getFileString();

	FileReader::MessageVec getMessages();

};
#endif /* FILEREADER_H */